# -*- coding: utf-8 -*-
from collections import deque


def shift_list(list_to_shift: list, places_to_shift: int) -> list:
    """
    A simple function that takes a list and a number indicating the number of places to shift the items within the list.
    When a positive number is passed in for the number of places to shift, the function will shift items left, whereas
    if a negative number is passed in, then the function will shift items right.

    :param list_to_shift: (List) - The list that will be have its items shifted
    :param places_to_shift: (Int) - The number of places to shift the items of the list left (positive) or
                            right (negative)
    :return: (List) - This function will return a list of the same length as passed in, but with a different order
    """
    shift_deque = deque(list_to_shift)
    shift_deque.rotate(places_to_shift * -1)
    return list(shift_deque)
