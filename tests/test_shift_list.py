from unittest import TestCase

from src.shift_list import shift_list


class TestShiftList(TestCase):

    def test_happy_path(self):
        self.assertEqual([2, 3, 4, 1], shift_list([1, 2, 3, 4], 1))

    def test_mixed_type_list(self):
        self.assertEqual([[1, 2, 3], 1, 'test'], shift_list([1, 'test', [1, 2, 3]], 2))

    def test_shift_number_equals_list_size(self):
        self.assertEqual([1, 2, 3], shift_list([1, 2, 3], 3))

    def test_shift_number_larger_than_list_size(self):
        self.assertEqual([3, 1, 2], shift_list([1, 2, 3], 5))

    def test_shift_number_larger_than_multiple_of_list_size(self):
        self.assertEqual([3, 1, 2], shift_list([1, 2, 3], 23))

    def test_negative_shift_number(self):
        self.assertEqual([4, 1, 2, 3], shift_list([1, 2, 3, 4], -1))

    def test_negative_shift_number_equals_list_size(self):
        self.assertEqual([1, 2, 3], shift_list([1, 2, 3], -3))

    def test_negative_shift_number_larger_than_list_size(self):
        self.assertEqual([2, 3, 1], shift_list([1, 2, 3], -5))

    def test_negative_shift_number_larger_than_multiple_of_list_size(self):
        self.assertEqual([2, 3, 1], shift_list([1, 2, 3], -23))
